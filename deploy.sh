#!/bin/bash

docker login registry.gitlab.com
docker build -t registry.gitlab.com/krjordan/site-maintenance .
docker push registry.gitlab.com/krjordan/site-maintenance
